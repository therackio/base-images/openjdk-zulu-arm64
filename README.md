# openjdk-zulu-arm64

ARM64 container with Azul Zulu OpenJDK installed using some of the better practices from OpenJDK containers.



See https://www.azul.com/downloads/zulu-community/?architecture=arm-64-bit&package=jdk for more information

